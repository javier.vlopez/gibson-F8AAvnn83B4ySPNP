# Documentation
# Solution

> Note: A Gitlab repository is required for this specific solution, as flux depends on a repository to mirror the kubernetes state into, and it has been configured to do so in gitlab. [It can easily be changed if necessary](https://toolkit.fluxcd.io/cmd/flux_bootstrap_github/)

> Note: The google service account email is printed in plain text, and that contains the google project id and the service account email `./flux/apps/cluster-01/ingress-system/external-dns-helmrelease.yaml`. Ideally this should be omitted. 

If you want to run this in your own GCP + Gitlab repo, clone or fork this repository and do the following:

1. Delete the path `./flux/clusters/cluster-01` 
1. In the file `./terraform/live/terraform.tfvars` change references to your own gitlab setup 
1. In the file `./flux/apps/cluster-01/ingress-system/external-dns-helmrelease.yaml` change the serviceAccount.annotations to your serviceAccount sa email, that is after running terrform. 
1. Proceed with the next section

### 1 Create a GKE Cluster using Terraform 

**Relevant files**

```bash
./terraform/live      # Contains all the terraform code.
```

Requirements: 

* [GCP project](https://cloud.google.com/appengine/docs/standard/nodejs/building-app/creating-project)
* [Gitlab API Access Token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html)
* [gcloud sdk - 307.0.0](https://cloud.google.com/sdk/docs/install)
* [kubectl - client v1.20.1](https://kubernetes.io/docs/tasks/tools/install-kubectl/) 
* [terraform - v0.14.6](https://www.terraform.io/downloads.html)

The terraform files are contained in the `./terraform/live` directory. It lets you create a minimal GKE cluster with default networking, using local state (note that when working in a production ready environment, this would be in remote, and using more industry standard practices).

To authenticate, simple authentication was chosen. Run the following:

```bash
gcloud auth application-default login
```

Copy the file `./terraform/live/env.sh.template` and paste your credentials and gcp project id into:

```bash
cp ./terraform/live/env.sh.template ./terraform/live/env.sh
# Paste your Gitlab Access Token with API permissions and GCP Project id accordingly
source ./terraform/live/env.sh
```

To create the cluster, run the following terraform commands:

```bash
( cd ./terraform/live && terraform init && terraform apply -auto-approve )
```

To connect to the cluster:

```bash 
gcloud container clusters get-credentials cluster-01 --region=$(cd ./terraform/live && terraform output -raw cluster_region)
# Check the connection
kubectl get nodes
```

### 2 Create an app

**Relevant files**

```bash
- ./applications/api    # Golang simple application and a Dockerfile to build the image
```

Requirements: 

* [docker](https://docs.docker.com/get-docker/)

The application is contained in the `applications/api` directory. It is a very basic **golang** application (no tests are included, no connection to database, the application serves data from variables). There are 2 available endpoints: 

- `/health`: used for kubernetes readiness / liveness. This is to exemplify what a real life application would require as a bare minimal to ensure auto-recovery and self-healing.
- `/api/products/`: as per requirements, it will respond with a JSON containing a fixed set of products (copy from `https://reqres.in/api/products/`).

A file called `Versionfile` has been included for basic versioning.

> Note: In a real world scenario, a pipeline would build, test, scan for vulnerabilities and finally upload / deploy the image into a registry.

To manually build the image, simply run the following command:

```bash
docker build applications/api -t api:$(cat applications/api/Versionfile)
```

To facilitate my own testing, I have uploaded the image in the following public registry:

```bash
## https://hub.docker.com/repository/docker/unodos/test-api 
unodos/test-api
```

#### Required Environment Variables

* `APP_PORT`: the port the application will listen in

### 3 Deploy the app 

**Relevant files**

```bash
- ./charts/generic-app                     # Chart used to deploy the application
- ./flux/apps/cluster-01/image-automation  # Flux image automation files, to 
                                           # auto deploy app images based on semver
- ./flux/apps/cluster-01/dev               # App helm release is located here
- ./flux/apps/ingress-system               # Nginx + Cert-manager + External-dns helm
                                           # releases. Helps with creation of ingresses
```

Requirements:

* [flux 2](https://toolkit.fluxcd.io/)

The application is deployed using Flux 2 and Helm 3. The reason for chosing these tools is that I really believe is the fastest and simplest way to achieve a fully automated and reliable kubernetes deployment process. 

A helm chart has been added at `./chart/generic-app`. The idea is that this chart would be used to deploy the application(s) in the Kubernetes cluster. it consists on a basic deployment, service, ingress and hpa configuration.

To actually deploy the application, terraform bootstraps flux 2 and lets it mirror the kubernetes state into a git repository (the `./flux/clusters/cluster-01` directory). Now, a push of a new tag of the docker image of the applcation would trigger flux and this would perform an update in the cluster thanks to the image-automation-controller. For more information about this visit [this link](https://toolkit.fluxcd.io/guides/image-update/). 

The files located at `./flux/apps/cluster-01/dev/` manage the deployment of the api. The following fragment, contains the marker flux needs in order to update the image:

```yaml
# ./flux/apps/cluster-01/dev/api-helmrelease.yaml
      image:
        name: unodos/psns-api
        tag: 0.0.0 # {"$imagepolicy": "flux-system:dev-api:tag"}
```

What I understood about the sentence `Create an app that accepts response from : https://reqres.in/api/products/`, is that there would be an ingress created in the cluster with the host set to `reqres.io` and that path `/api/products`. My solution was to deploy cert-manager, nginx-ingress-controller and external-dns and use a delegated zone called `gcp.unodoscorp.com`, and to create the ingress with that host. Although, it may not be the desired solution. 

### 4. Enable Auto scaling and Test

**Relevant files**

```bash
- ./terraform/live/main.tf            # cluster autoscaler enabled in the node pool
- ./tests/load/load-generator-rs.yaml # replicaset used to test cluster autoscaler
```

Cluster Autoscaler is enabled in the node pool level in terraform:

```hcl
resource "google_container_node_pool" "cluster" {
  # ...
  autoscaling {
    min_node_count = 1
    max_node_count = 3
  }
  # ...
}
```

To check the status of cluster-autoscaler at any stage, run following:

```bash
kubectl get configmap cluster-autoscaler-status --namespace kube-system -o yaml
```

To test autoscaling, I followed the [documentation available in the kubernetes site](https://kubernetes.io/docs/tasks/run-application/horizontal-pod-autoscale-walkthrough/). There are many ways to test this is just one of them, simplest would be to increase the number of nodes in the api helmrelease. This would work as Horizontal Pod Autoscaler is enabled by default in GKE and the application helm release has an HPA configuration.

```bash
kubectl apply -f ./tests/load/load-generator-rs.yaml
```

After doing this, run the following commands to check whether it is working, in separate terminals if possible:

```bash
# Check for hpa and autoscaler events 
kubectl get events --all-namespaces --watch | grep autoscaler
# Check hpa status
kubectl get hpa --namespace dev --watch
# Check the nodes
kubectl get nodes --watch 
# Check api pods
kubectl get pods --namespace dev --watch 
```

To remove the load-generator, do the following:

```bash
kubectl delete -f ./tests/load/load-generator-rs.yaml
```

### 5. Cleanup 

To delete the cluster and cleanup, run the following:

```bash
terraform destroy -auto-approve
```

## References 

* [Golang Environment Variables](https://www.loginradius.com/blog/async/environment-variables-in-golang/)
* [What is Gitops?](https://www.weave.works/technologies/gitops/)
* [Nginx and External DNS in GCP](https://github.com/kubernetes-sigs/external-dns/blob/master/docs/tutorials/nginx-ingress.md)
* [Subdomain delegation from AWS to GCP](https://stackoverflow.com/questions/54915334/point-and-use-a-subdomain-in-gcp)
