variable "google_project_id" {
  description = "Google Cloud Platform Project ID. Declare as ENV VAR"
}

variable "region" {
  description = "LocatiRegionon of the gke cluster"
  default     = "europe-west1"
}

variable "locations" {
  description = "Location of the gke cluster"
  type        = list(string)
  default     = ["europe-west1-b", "europe-west1-c", "europe-west1-d"]
}

variable "cluster_name" {
  description = "Kubernetes cluster name"
}

variable "gitlab_token" {
  description = "GITLAB_TOKEN to initialise flux. Declare as ENV VAR"
}

variable "flux_repo_owner" {
  description = "Gitlab repository owner"
}

variable "flux_repo_name" {
  description = "Repository name"
}

variable "flux_repo_branch" {
  description = "git branch name to configure"
}

variable "flux_repo_path" {
  description = "Directory path to use for bootsraping within repository branch"
}
