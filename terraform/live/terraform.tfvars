# Declare as environment variables
# google_project_id =
# gitlab_token = 

# Cluster 
region       = "europe-west1"
location     = "europe-west1-b"
cluster_name = "cluster-01"

# Flux bootstrap 
flux_repo_owner  = "javier.vlopez"
flux_repo_name   = "gibson-F8AAvnn83B4ySPNP"
flux_repo_branch = "master"
flux_repo_path   = "flux/clusters/cluster-01"
