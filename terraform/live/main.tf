provider "google" {
  project = var.google_project_id
  region  = var.region
}

resource "google_service_account" "sa" {
  account_id   = "service-account-psns"
  display_name = "PSNS Service Account"
}

resource "google_project_iam_binding" "dns_admin" {
  project = var.google_project_id
  role    = "roles/dns.admin"
  members = [
    "serviceAccount:${google_service_account.sa.email}",
  ]
}

data "google_iam_policy" "iam_workload_id_user" {
  binding {
    role = "roles/iam.workloadIdentityUser"

    members = [
      "serviceAccount:${var.google_project_id}.svc.id.goog[ingress-system/external-dns]",
    ]
  }
}

resource "google_service_account_iam_policy" "iam_workload_id_user" {
  service_account_id = google_service_account.sa.name
  policy_data        = data.google_iam_policy.iam_workload_id_user.policy_data
}

resource "google_container_cluster" "cluster" {
  name                     = var.cluster_name
  location                 = var.region
  remove_default_node_pool = true
  initial_node_count       = 1
  node_locations           = var.locations

  workload_identity_config {
    identity_namespace = "${var.google_project_id}.svc.id.goog"
  }
}

resource "google_container_node_pool" "cluster" {
  name               = "${var.cluster_name}-node-pool"
  location           = var.region
  cluster            = google_container_cluster.cluster.name
  initial_node_count = 1

  autoscaling {
    min_node_count = 1
    max_node_count = 3
  }

  node_config {
    preemptible     = false
    machine_type    = "e2-medium"
    service_account = google_service_account.sa.email
    disk_size_gb    = 10
    disk_type       = "pd-standard"
    oauth_scopes = [
      "https://www.googleapis.com/auth/cloud-platform",
      "https://www.googleapis.com/auth/ndev.clouddns.readwrite"
    ]
    workload_metadata_config {
      node_metadata = "GKE_METADATA_SERVER"
    }
  }
}

module "cluster_auth" {
  source = "terraform-google-modules/kubernetes-engine/google//modules/auth"

  project_id   = var.google_project_id
  cluster_name = var.cluster_name
  location     = google_container_cluster.cluster.location
  depends_on   = [google_container_cluster.cluster, google_container_node_pool.cluster]
}

resource "local_file" "kubeconfig" {
  content  = module.cluster_auth.kubeconfig_raw
  filename = "${path.module}/kubeconfig"
}

provider "kubernetes" {
  load_config_file       = false
  cluster_ca_certificate = module.cluster_auth.cluster_ca_certificate
  host                   = module.cluster_auth.host
  token                  = module.cluster_auth.token
}

resource "null_resource" "flux_init" {
  triggers = {
    once = timestamp()
  }

  provisioner "local-exec" {

    command = <<-EOF
      flux bootstrap gitlab \
        --components-extra=image-reflector-controller,image-automation-controller \
        --watch-all-namespaces \
        --context=$CONTEXT \
        --owner=$REPO_OWNER \
        --repository=$REPO_NAME \
        --branch=$REPO_BRANCH \
        --private \
        --token-auth \
        --path=$REPO_PATH
    EOF

    environment = {
      CONTEXT      = var.cluster_name
      REPO_OWNER   = var.flux_repo_owner
      REPO_NAME    = var.flux_repo_name
      REPO_BRANCH  = var.flux_repo_branch
      REPO_PATH    = var.flux_repo_path
      GITLAB_TOKEN = var.gitlab_token
      KUBECONFIG   = "${path.module}/kubeconfig"
    }
  }

  depends_on = [local_file.kubeconfig]
}
