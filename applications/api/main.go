package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"time"
)

// Fixed return values
// fixedResponseProducts should be looking for data in a database for example
const fixedResponseProducts = `{"page":1,"per_page":6,"total":12,"total_pages":2,"data":[{"id":1,"name":"cerulean","year":2000,"color":"#98B2D1","pantone_value":"15-4020"},{"id":2,"name":"fuchsia rose","year":2001,"color":"#C74375","pantone_value":"17-2031"},{"id":3,"name":"true red","year":2002,"color":"#BF1932","pantone_value":"19-1664"},{"id":4,"name":"aqua sky","year":2003,"color":"#7BC4C4","pantone_value":"14-4811"},{"id":5,"name":"tigerlily","year":2004,"color":"#E2583E","pantone_value":"17-1456"},{"id":6,"name":"blue turquoise","year":2005,"color":"#53B0AE","pantone_value":"15-5217"}],"support":{"url":"https://reqres.in/#support-heading","text":"To keep ReqRes free, contributions towards server costs are appreciated!"}}`

// fixedResponseHealth should be checking if the connection is fine with the database for example
const fixedResponseHealth = `{"status": "OK", "message": "I am healthy"}`

func HttpLog(req *http.Request) {
	f := map[string]interface{}{
		"api_time":       time.Now().Format(time.RFC3339),
		"api_method":     req.Method,
		"api_proto":      req.Proto,
		"api_host":       req.Host,
		"api_requestUri": req.RequestURI,
		"api_userAgent":  req.UserAgent(),
	}
	jsonData, _ := json.Marshal(f)
	log.Println(string(jsonData))
}

func GetProducts(rw http.ResponseWriter, req *http.Request) {
	HttpLog(req)
	rw.Header().Set("Content-Type", "application/json")
	rw.Write([]byte(fixedResponseProducts))
}

func GetHealth(rw http.ResponseWriter, req *http.Request) {
	HttpLog(req)
	rw.Header().Set("Content-Type", "application/json")
	rw.Write([]byte(fixedResponseHealth))
}

func main() {
	log.SetFlags(0)
	log.SetOutput(os.Stdout)

	// Configuration, coming from environment variables typically
	port, ok := os.LookupEnv("APP_PORT")
	if !ok {
		log.Fatal("The environment variable APP_PORT was not defined")
	}

	// Function handlers, should handle specific methods
	http.HandleFunc("/api/products", GetProducts)
	http.HandleFunc("/health", GetHealth)

	// Start and serve in the specific configured port
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%s", port), nil))
}
